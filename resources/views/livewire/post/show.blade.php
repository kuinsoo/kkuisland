<div>
    <!-- The Master doesn't talk, he acts. -->
    <h3>{{ $this->title }}</h3>
    <p>{{ $this->content }}</p>
    <button wire:click="like">Like Post</button>
    <p>{{ $this->user->identification ?? '없음' }}</p>
    <div>menu</div>
</div>
