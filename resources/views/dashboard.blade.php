<x-kkuapp-layout>
    @section('css')
    {{-- begin::Page Custom Styles(used by this page) --}}
    <link href="assets/plugins/custom/kanban/kanban.bundle.css" rel="stylesheet" type="text/css" />
    {{-- end::Page Custom Styles --}}
    @endsection
    {{-- <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot> --}}

    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">하루 일과</h3>
            </div>
        </div>
        <div class="card-body">
            <div id="kt_kanban_2"></div>
        </div>
        <div class="card-body">
            <div id="kt_kanban_4"></div>
            <div class="mt-4">
                <button class="btn font-weight-bold btn-light-primary mr-5" id="addDefault">Add "Default" board</button>
                <button class="btn font-weight-bold btn-light-danger mr-5" id="addToDo">Add element in "To Do" Board</button>
                <button class="btn font-weight-bold btn-light-success" id="removeBoard">Remove "Done" Board</button>
            </div>
        </div>
    </div>
    <!--end::Card-->


    @section('script')
    {{-- begin::Page Scripts(used by this page) --}}
    <script src="assets/plugins/custom/kanban/kanban.bundle.js"></script>
    {{-- <script src="assets/js/pages/features/miscellaneous/kanban-board.js"></script> --}}
    <script>
        var kanban = new jKanban({
            element: '#kt_kanban_2',
            gutter: '0',
            widthBoard: '250px',
            boards: [{
                'id': '_inprocess',
                'title': '계획',
                'class': 'primary',
                'item': [{
                    'title': '<span class="font-weight-bold">꾸인수</span>',
                    'class': 'light-primary',
                },
                {
                    'title': '<span class="font-weight-bold">미소뭐해?</span>',
                    'class': 'light-primary',
                }
                ]
            }, {
                'id': '_working',
                'title': '실행',
                'class': 'success',
                'item': [{
                    'title': '<span class="font-weight-bold">홈페이지 개발</span>',
                    'class': 'light-success',
                },
                {
                    'title': '<span class="font-weight-bold">구경하기</span>',
                    'class': 'light-success',
                }
                ]
            }, {
                'id': '_done',
                'title': '완료',
                'class': 'danger',
                'item': [{
                    'title': '<span class="font-weight-bold">미소 산책!</span>',
                    'class': 'light-danger',
                },
                {
                    'title': '<span class="font-weight-bold">혼자 살기!</span>',
                    'class': 'light-danger',
                }
                ]
            }
            ],
            itemAddOptions: {
                enabled: false,                                              // add a button to board for easy item creation
                content: '+',                                                // text or html content of the board button
                class: 'kanban-title-button btn btn-default btn-xs',         // default class of the button
                footer: false                                                // position the button on footer
            },
            itemHandleOptions: {
                enabled             : false,                                 // if board item handle is enabled or not
                handleClass         : "item_handle",                         // css class for your custom item handle
                customCssHandler    : "drag_handler",                        // when customHandler is undefined, jKanban will use this property to set main handler class
                customCssIconHandler: "drag_handler_icon",                   // when customHandler is undefined, jKanban will use this property to set main icon handler class. If you want, you can use font icon libraries here
                customHandler       : "<span class='item_handle'>+</span> %s"// your entirely customized handler. Use %s to position item title
            },
            click            : function (el) {console.log('click')},                             // callback when any board's item are clicked
            dragEl           : function (el, source) {console.log('drag')},                     // callback when any board's item are dragged
            dragendEl        : function (el) {console.log('drop')},                             // callback when any board's item stop drag
            dropEl           : function (el, target, source, sibling) {console.log('dragBoard');},    // callback when any board's item drop in a board
            dragBoard        : function (el, source) {console.log('upBoard');},                     // callback when any board stop drag
            dragendBoard     : function (el) {console.log('dropBard');},                             // callback when any board stop drag
            buttonClick      : function(el, boardId) {console.log("버튼클릭");},                      // callback when the board's button is clicked
        });

        var toDoButton = document.getElementById('addToDo');
        toDoButton.addEventListener('click', function ()
        {
            kanban.addElement(
                '_inprocess', {
                'title': `
                        <div class="d-flex align-items-center">
                            <div class="symbol symbol-light-primary mr-3">
                                <img alt="Pic" src="assets/media/users/300_14.jpg" />
                            </div>
                            <div class="d-flex flex-column align-items-start">
                                <span class="text-dark-50 font-weight-bold mb-1">Requirement Study</span>
                                <span class="label label-inline label-light-success font-weight-bold">Scheduled</span>
                            </div>
                        </div>
                    `
            }
            );
        });

        var addBoardDefault = document.getElementById('addDefault');
        addBoardDefault.addEventListener('click', function ()
        {
            kanban.addBoards(
                [{
                    'id': '_default',
                    'title': 'New Board',
                    'class': 'primary-light',
                    'item': [{
                        'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_13.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                        <span class="text-dark-50 font-weight-bold mb-1">Payment Modules</span>
                                        <span class="label label-inline label-light-primary font-weight-bold">In development</span>
                                    </div>
                                </div>
                        `}, {
                        'title': `
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-success mr-3">
                                        <img alt="Pic" src="assets/media/users/300_12.jpg" />
                                    </div>
                                    <div class="d-flex flex-column align-items-start">
                                    <span class="text-dark-50 font-weight-bold mb-1">New Project</span>
                                    <span class="label label-inline label-light-danger font-weight-bold">Pending</span>
                                </div>
                            </div>
                        `}
                    ]
                }]
            )
        });

        var removeBoard = document.getElementById('removeBoard');
        removeBoard.addEventListener('click', function ()
        {
            kanban.removeBoard('_done');
        });

    </script>
    {{-- end::Page Scripts --}}
    @endsection
</x-kkuapp-layout>
