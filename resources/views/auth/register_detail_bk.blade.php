<x-guest-layout>
    <link href="assets/css/pages/login/login-3.css" rel="stylesheet" type="text/css" />

    <div class="d-flex flex-column flex-root">
        <!-- begin::Login -->
        <div class="login login-3 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login">
            <!-- begin::Aside -->
            <div class="login-aside d-flex flex-column flex-row-auto">
                <!-- begin::Aside Top -->
                <div class="d-flex flex-column-auto flex-column pt-15 px-30">
                    <!-- begin::Aside header -->
                    <a href="#" class="login-logo py-6">
                        <img src="assets/media/logos/logo-1.png" class="max-h-70px" alt="" />
                    </a>
                    <!-- end::Aside header -->
                    <!-- begin: Wizard Nav -->
                    <div class="wizard-nav pt-5 pt-lg-30">
                        <!-- begin::Wizard Steps -->
                        <div class="wizard-steps">
                            <!-- begin::Wizard Step 1 Nav -->
                            <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                <div class="wizard-wrapper">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">1</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">{{__('Account Settings')}}</h3>
                                        <div class="wizard-desc">{{__('Setup Your Account Details')}}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- end::Wizard Step 1 Nav -->
                            <!-- begin::Wizard Step 2 Nav -->
                            <div class="wizard-step" data-wizard-type="step">
                                <div class="wizard-wrapper">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">2</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">{{__('Address Details')}}</h3>
                                        <div class="wizard-desc">{{__('Setup Residence Address')}}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- end::Wizard Step 2 Nav -->

                            <!-- begin::Wizard Step 3 Nav -->
                            <div class="wizard-step" data-wizard-type="step">
                                <div class="wizard-wrapper">
                                    <div class="wizard-icon">
                                        <i class="wizard-check ki ki-check"></i>
                                        <span class="wizard-number">3</span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">{{__('Completed')}}!</h3>
                                        <div class="wizard-desc">{{__('Review and Submit')}}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- end::Wizard Step 3 Nav -->
                        </div>
                        <!-- end::Wizard Steps -->
                    </div>
                    <!-- end: Wizard Nav -->
                </div>
                <!-- end::Aside Top -->
                <!-- begin::Aside Bottom -->
                <div class="aside-img-wizard d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center pt-2 pt-lg-5" style="background-position-y: calc(100% + 3rem); background-image: url(assets/media/svg/illustrations/features.svg)"></div>
                <!-- end::Aside Bottom -->
            </div>
            <!-- begin::Aside -->
            <!-- begin::Content -->
            <div class="login-content flex-column-fluid d-flex flex-column p-10">
                <!-- begin::Top -->
                <div class="text-right d-flex justify-content-center">
                    <div class="top-signup text-right d-flex justify-content-end pt-5 pb-lg-0 pb-10">
                        <span class="font-weight-bold text-muted font-size-h4">{{__('Having issues')}}?</span>
                        <a href="javascript:;" class="font-weight-bolder text-primary font-size-h4 ml-2" id="kt_login_signup">{{__('Get Help')}}</a>
                    </div>
                </div>
                <!-- end::Top -->
                <!-- begin::Wrapper -->
                <div class="d-flex flex-row-fluid flex-center">
                    <!-- begin::Signin -->
                    <div class="login-form login-form-signup">
                        <!-- begin::Form -->
                        <form action="{{ route('register') }}" class="form" novalidate="novalidate" id="kt_login_signup_form" method="POST">
                            @csrf
                            <!-- begin: Wizard Step 1 -->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <!-- begin::Title -->
                                <div class="pb-10 pb-lg-15">
                                    <h3 class="font-weight-bolder text-dark display5">{{__('Create Account')}}</h3>
                                    <div class="text-muted font-weight-bold font-size-h4">{{__('Already have an Account')}} ?
                                    <a href="custom/pages/login/login-3/signin.html" class="text-primary font-weight-bolder">{{__('Login')}}</a></div>
                                </div>
                                <!-- begin::Title -->
                                <!-- begin::Form Group -->
                                <div class="form-group">
                                    <label class="font-size-h6 font-weight-bolder text-dark">{{__('ID')}}</label>
                                    <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" onkeyup="copyComplete(event);" name="identification" placeholder="{{__('ID')}}" value="" />
                                </div>

                                <div class="form-group">
                                    <label for="password" class="font-size-h6 font-weight-bolder text-dark">{{__('Password')}}</label>
                                    <input type="password" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="password" name="password" autocomplete="new-password" required/>
                                </div>

                                <div class="form-group">
                                    <label for="password_confirmation" class="font-size-h6 font-weight-bolder text-dark">{{__('Confirm Password')}}</label>
                                    <input type="password" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="password_confirmation" name="password_confirmation" autocomplete="new-password" required/>
                                </div>
                                <!-- end::Form Group -->
                                <!-- begin::Form Group -->
                                <div class="form-group">
                                    <label class="font-size-h6 font-weight-bolder text-dark">{{__('Name')}}</label>
                                    <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" onkeyup="copyComplete(event);" name="name" placeholder="{{__('Name')}}" value="" />
                                </div>
                                <!-- end::Form Group -->
                                <!-- begin::Form Group -->
                                <div class="form-group">
                                    <label class="font-size-h6 font-weight-bolder text-dark">{{__('Phone')}}</label>
                                    <input type="tel" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" onkeyup="copyComplete(event);" name="phone" placeholder="{{__('Phone')}}" value="" />
                                </div>
                                <!-- end::Form Group -->
                                <!-- begin::Form Group -->
                                <div class="form-group">
                                    <label class="font-size-h6 font-weight-bolder text-dark">{{__('Email')}}</label>
                                    <input type="email" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" onkeyup="copyComplete(event);" name="email" placeholder="{{__('Email')}}" value="" />
                                </div>
                                <!-- end::Form Group -->
                            </div>
                            <!-- end: Wizard Step 1 -->
                            <!-- begin: Wizard Step 2 -->
                            <div class="pb-5" data-wizard-type="step-content">
                                <!-- begin::Title -->
                                <div class="pt-lg-0 pt-5 pb-15">
                                    <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">{{__('Address Details')}}</h3>
                                    <div class="text-muted font-weight-bold font-size-h4">{{__('Have a Different Address')}} ?
                                    <a href="javascript:void(0);" onclick="openPostcode()" class="text-primary font-weight-bolder">{{__('Add Address')}}</a></div>
                                </div>

                                <div id="wrap" style="display:none;border:1px solid;width:500px;height:300px;margin:5px 0;position:relative">
                                    <img src="//t1.daumcdn.net/postcode/resource/images/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="closePostcode()" alt="접기 버튼">
                                </div>
                                <!-- begin::Title -->

                                   <!-- begin::Row -->
                                   <div class="row">
                                    <div class="col-xl-4">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('City')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="id-sido" name="sido" placeholder="{{__('City')}}" value="" readonly/>
                                        </div>
                                        <!-- end::Input -->
                                    </div>
                                    <div class="col-xl-4">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('State')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="id-sigungu" name="sigungu" placeholder="{{__('State')}}" value="" readonly/>
                                        </div>
                                        <!-- end::Input -->
                                    </div>
                                    <div class="col-xl-4">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('Postcode')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="id-postcode" name="postcode" placeholder="{{__('Postcode')}}" value="" readonly/>
                                        </div>
                                        <!-- end::Input -->
                                    </div>
                                </div>
                                <!-- end::Row -->

                                <!-- begin::Row -->
                                <div class="row">
                                    <div class="col-xl-12">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('Address Line 1')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="id-address" name="address1" placeholder="{{__('Address Line 1')}}" value="" readonly/>
                                        </div>
                                        <!-- end::Input -->
                                    </div>
                                    <div class="col-xl-12">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('Address Line 2')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" id="id-extraAddress" name="address2" placeholder="{{__('Address Line 2')}}" value="" readonly/>
                                        </div>
                                        <!-- end::Input -->
                                    </div>
                                </div>
                                <!-- end::Row -->

                                <!-- begin::Row -->
                                <div class="row">
                                    <div class="col-xl-12">
                                        <!-- begin::Input -->
                                        <div class="form-group">
                                            <label class="font-size-h6 font-weight-bolder text-dark">{{__('Address Detail')}}</label>
                                            <input type="text" class="form-control h-auto py-7 px-6 border-0 rounded-lg font-size-h6" onkeyup="copyComplete(event);" id="id-detailAddress" name="addressdetail" placeholder="{{__('Address Detail')}}" value="" />
                                            <span class="form-text text-muted">{{__('Please enter your State')}}.</span>
                                        </div>
                                        <!-- end::Input -->
                                    </div>

                                </div>
                                <!-- end::Row -->
                            </div>
                            <!-- end: Wizard Step 2 -->

                            <!-- begin: Wizard Step 3 -->
                            <div class="pb-5" data-wizard-type="step-content">
                                <!-- begin::Title -->
                                <div class="pt-lg-0 pt-5 pb-15">
                                    <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">{{__('Complete')}}</h3>
                                    <div class="text-muted font-weight-bold font-size-h4">{{__('Complete Your Signup And Become A Member')}}</div>
                                </div>
                                <!-- end::Title -->
                                <!-- begin::Section -->
                                <h4 class="font-weight-bolder mb-3">{{__('Accoun Settings')}}:</h4>
                                <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                    <div id="identification"></div>
                                    <div id="name"></div>
                                    <div id="phone"></div>
                                    <div id="email"></div>
                                </div>
                                <!-- end::Section -->
                                <!-- begin::Section -->
                                <h4 class="font-weight-bolder mb-3">{{__('Address Details')}}:</h4>
                                <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                    <div id="sido"></div>
                                    <div id="sigungu"></div>
                                    <div id="postcode"></div>
                                    <div id="address1"></div>
                                    <div id="address2"></div>
                                    <div id="addressdetail"></div>
                                </div>
                                <!-- end::Section -->
                            </div>
                            <!-- end: Wizard Step 3 -->
                            <!-- begin: Wizard Actions -->
                            <div class="d-flex justify-content-between pt-3">
                                <div class="mr-2">
                                    <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pl-6 pr-8 py-4 my-3 mr-3" data-wizard-type="action-prev">
                                    <span class="svg-icon svg-icon-md mr-1">
                                        <!-- begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg -->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1" />
                                                <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)" />
                                            </g>
                                        </svg>
                                        <!-- end::Svg Icon -->
                                    </span>{{__('Previous')}}</button>
                                </div>
                                <div>
                                    <button class="btn btn-primary font-weight-bolder font-size-h6 pl-5 pr-8 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">{{__('Submit')}}
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <!-- begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg -->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24" />
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                            </g>
                                        </svg>
                                        <!-- end::Svg Icon -->
                                    </span></button>
                                    <button type="button" class="btn btn-sm btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">{{__('Next Step')}}
                                        <span class="svg-icon svg-icon-md ml-1">
                                            <!-- begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg -->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                                                    <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!-- end::Svg Icon -->
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <!-- end: Wizard Actions -->
                        </form>
                        <!-- end::Form -->
                    </div>
                    <!-- end::Signin -->
                </div>
                <!-- end::Wrapper -->
            </div>
            <!-- end::Content -->
        </div>
        <!-- end::Login -->
    </div>

    <script src="assets/js/pages/custom/login/login-3.js"></script>
    <script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
    <script>

    // 회원가입 마지막 화면 확인 목적
    function copyComplete(e) {
        var targetName = e.target.name;
        var targetValue = e.target.value;
        var targetItem = document.getElementById(targetName);
        targetItem.innerHTML = targetValue;
        console.log(targetName,targetValue, targetItem.innerHTML);
    }

    // 우편번호 찾기 찾기 화면을 넣을 element
    var element_wrap = document.getElementById('wrap');

    // 우편번호 찾기 닫기
    function closePostcode() {
        // iframe을 넣은 element를 안보이게 한다.
        element_wrap.style.display = 'none';
    }

    // 우편번호 찾아 열기
    function openPostcode() {
        // 현재 scroll 위치를 저장해놓는다.
        var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
        new daum.Postcode({
            oncomplete: function(data) {
                // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수
                var sido = data.sido; // 시
                var sigungu = data.sigungu; // 시군구

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
                if(data.userSelectedType === 'R'){
                    // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                    // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                    if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있고, 공동주택일 경우 추가한다.
                    if(data.buildingName !== '' && data.apartment === 'Y'){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                    if(extraAddr !== ''){
                        extraAddr = ' (' + extraAddr + ')';
                    }
                    // 조합된 참고항목을 해당 필드에 넣는다.
                    document.getElementById("id-extraAddress").value = extraAddr;
                    document.getElementById("address2").innerHTML = extraAddr;

                } else {
                    document.getElementById("id-extraAddress").value = '';
                    document.getElementById("address2").innerHTML = '';
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('id-postcode').value = data.zonecode;
                document.getElementById("id-address").value = addr;
                document.getElementById("id-sido").value = sido;
                document.getElementById("id-sigungu").value = sigungu;

                document.getElementById('postcode').innerHTML = data.zonecode;
                document.getElementById("address1").innerHTML = addr;
                document.getElementById("sido").innerHTML = sido;
                document.getElementById("sigungu").innerHTML = sigungu;
                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("id-detailAddress").focus();

                // iframe을 넣은 element를 안보이게 한다.
                // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
                element_wrap.style.display = 'none';

                // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
                document.body.scrollTop = currentScroll;
            },
            // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
            onresize : function(size) {
                element_wrap.style.height = size.height+'px';
            },
            width : '100%',
            height : '100%'
        }).embed(element_wrap);

        // iframe을 넣은 element를 보이게 한다.
        element_wrap.style.display = 'block';
    }
    </script>
</x-guest-layout>
