<x-guest-layout>
    <link href="assets/css/pages/login/classic/login-4.css" rel="stylesheet" type="text/css" />
    <!-- begin::Main -->
    <div class="d-flex flex-column flex-root">
        <!-- begin::Login -->
        <div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat" style="background-image: url('assets/media/bg/bg-3.jpg');">
                <div class="login-form text-center p-7 position-relative overflow-hidden">
                    <!-- begin::Login Header -->
                    <x-logo-header />
                    <!-- end::Login Header -->
                    @if (session('status'))
                        <div class="mb-4 font-medium text-sm text-green-600">
                            {{ session('status') }}
                        </div>
                    @endif

                    <x-jet-validation-errors class="mb-4" />

                    <!-- begin::Login Sign up form -->
                    <div class="login-signup" style="display: inline;">
                        <div class="mb-20">
                            <h3>{{__('Sign Up')}}</h3>
                            <div class="text-muted font-weight-bold">{{__('Enter your details to create your account')}}</div>
                        </div>


                        <form method="POST" action="{{ route('password.update') }}" class="form" id="kt_login_signup_form">
                            @csrf
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="email" placeholder="{{__('Email')}}" id="email" name="email"  :value="old('email', $request->email)" autocomplete="off" required />
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Password')}}" id="password" name="password" autocomplete="new-password" required />
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Confirm Password')}}" id="password_confirmation" name="password_confirmation" autocomplete="new-password" required />
                            </div>

                            <div class="form-group d-flex flex-wrap flex-center mt-10">
                                <button id="kt_login_signup_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Reset Password')}}</button>
                                <!-- <a href="{{ route('login') }}" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Cancel')}}</a> -->
                            </div>
                        </form>
                    </div>
                    <!-- end::Login Sign up form -->
                </div>
            </div>
        </div>
        <!-- end::Login -->
    </div>


    <script>
        var KTLogin = function ()
        {
            var _login;

            var _showForm = function (form)
            {
                var cls = 'login-' + form + '-on';
                var form = 'kt_login_' + form + '_form';

                _login.removeClass('login-signup-on');

                _login.addClass(cls);

                KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
            }

            var _handleSignUpForm = function (e)
            {
                var validation;
                var form = KTUtil.getById('kt_login_signup_form');

                if (!form)
                {
                    return;
                }
                // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
                validation = FormValidation.formValidation(
                    form,
                    {
                        fields: {
                            password: {
                                validators: {
                                    notEmpty: {
                                        // message: 'Last Name is required'
                                        message: '비밀번호 입력 필요'
                                    }
                                }
                            },
                            password_confirmation: {
                                validators: {
                                    notEmpty: {
                                        // message: 'The password confirmation is required'
                                        message: '암호 확인이 필요합니다.'
                                    },
                                    identical: {
                                        compare: function ()
                                        {
                                            return form.querySelector('[name="password"]').value;
                                        },
                                        // message: 'The password and its confirm are not the same'
                                        message: '암호가 동일하지 않습니다.'
                                    }
                                }
                            },
                            email: {
                                validators: {
                                    notEmpty: {
                                        // message: 'Email is required'
                                        message: '이메일 입력 필요'
                                    },
                                    emailAddress: {
                                        // message: 'The value is not a valid email address'
                                        message: '올바른 이메일 주소가 아닙니다'
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap()
                        }
                    }
                );

                $('#kt_login_signup_submit').on('click', function (e)
                {
                    e.preventDefault();

                    validation.validate().then(function (status)
                    {
                        if (status == 'Valid')
                        {
                            swal.fire({
                                // text: "양식을 제출 하겠습니다.",
                                text: "양식을 제출 하겠습니다.",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "확인",
                                customClass: {
                                    confirmButton: "btn font-weight-bold btn-light-primary"
                                }
                            }).then(function ()
                            {
                                document.getElementById('kt_login_signup_form').submit();
                                // KTUtil.scrollTop();
                            });
                        } else
                        {
                            // text: "Sorry, looks like there are some errors detected, please try again.",
                            swal.fire({
                                text: "죄송합니다, 다시 시도해주세요.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "확인",
                                customClass: {
                                    confirmButton: "btn font-weight-bold btn-light-primary"
                                }
                            }).then(function ()
                            {
                                KTUtil.scrollTop();
                            });
                        }
                    });
                });

                // Handle cancel button
                $('#kt_login_signup_cancel').on('click', function (e)
                {
                    e.preventDefault();

                    _showForm('signin');
                });
            }

            // Public Functions
            return {
                // public functions
                init: function ()
                {
                    _login = $('#kt_login');
                    _handleSignUpForm();

                }
            };
        }();

        // Class Initialization
        jQuery(document).ready(function ()
        {
            KTLogin.init();
        });
    </script>

</x-guest-layout>
