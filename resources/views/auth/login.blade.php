<x-guest-layout>
    <link href="assets/css/pages/login/classic/login-4.css" rel="stylesheet" type="text/css" />
    <!-- begin::Main -->
    <div class="d-flex flex-column flex-root">
        <!-- begin::Login -->
        <div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat" style="background-image: url('assets/media/bg/bg-3.jpg');">
                <div class="login-form text-center p-7 position-relative overflow-hidden">
                    <!-- begin::Login Header -->
                    <x-logo-header />
                    <!-- end::Login Header -->
                    <!-- begin::Login Sign in form -->
                    <div class="login-signin">
                        <div class="mb-20">
                            <h3>{{__('Welcome To KKuIsland')}}</h3>
                            <div class="text-muted font-weight-bold">{{__('Enter your details to login to your account')}}</div>
                        </div>

                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif

                        <x-jet-validation-errors class="mb-4" />

                        <form  method="POST" action="{{ route('login') }}" class="form" id="kt_login_signin_form">
                            @csrf
                            <div class="form-group mb-5">
                                <input class="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="{{__('ID')}}" name="identification" autocomplete="off" />
                            </div>
                            <div class="form-group mb-5">
                                <input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Password')}}" name="password" />
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                                <div class="checkbox-inline">
                                    <label class="checkbox m-0 text-muted">
                                    <x-jet-checkbox id="remember_me" name="remember" />
                                    <span></span>{{__('Remember me')}}</label>
                                </div>
                                <a href="javascript:;" id="kt_login_forgot" class="text-muted text-hover-primary">{{__('Forgot Your Password?')}}</a>
                            </div>
                            <button id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">{{__('Sign In')}}</button>
                        </form>
                        <div class="mt-10">
                            <span class="opacity-70 mr-4">{{__("Don't have an account yet?")}}</span>
                            <a href="javascript:;" id="kt_login_signup" class="text-muted text-hover-primary font-weight-bold">{{__('Sign Up')}}!</a>
                        </div>
                    </div>
                    <!-- end::Login Sign in form -->
                    <!-- begin::Login Sign up form -->
                    <div class="login-signup">
                        <div class="mb-20">
                            <h3>{{__('Sign Up')}}</h3>
                            <div class="text-muted font-weight-bold">{{__('Enter your details to create your account')}}</div>
                        </div>


                        <form method="POST" action="{{ route('register') }}" class="form" id="kt_login_signup_form">
                            @csrf

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="{{__('ID')}}" id="identification" name="identification"  :value="old('identification')" required/>
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="text" placeholder="{{__('Name')}}" id="name" name="name" :value="old('name')" required/>
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="email" placeholder="{{__('Email')}}" id="email" name="email" :value="old('email')" autocomplete="off" required/>
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="tel" placeholder="{{__('Phone')}}" id="phone" name="phone" :value="old('phone')" autocomplete="off" required/>
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Password')}}" id="password" name="password" autocomplete="new-password" required />
                            </div>

                            <div class="form-group mb-5">
                                <x-jet-input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Confirm Password')}}" id="password_confirmation" name="password_confirmation" autocomplete="new-password" required />
                            </div>
                            <input type="hidden" name="sido" value="null">
                            <input type="hidden" name="sigungu" value="null">
                            <input type="hidden" name="postcode" value="null">
                            <input type="hidden" name="address1" value="null">
                            <input type="hidden" name="address2" value="null">
                            <input type="hidden" name="addressdetail" value="null">

                            <div class="form-group mb-5 text-left">
                                <div class="checkbox-inline">
                                    <label class="checkbox m-0">
                                    <x-jet-input type="checkbox" name="agree" />
                                    <span></span>{{__('I Agree the')}}
                                    <a href="#" class="font-weight-bold ml-1">{{__('terms and conditions')}}</a>.</label>
                                </div>
                                <div class="form-text text-muted text-center"></div>
                            </div>

                            <div class="form-group d-flex flex-wrap flex-center mt-10">
                                <button id="kt_login_signup_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Sign Up')}}</button>
                                <button id="kt_login_signup_cancel" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Cancel')}}</button>
                            </div>
                        </form>
                    </div>
                    <!-- end::Login Sign up form -->
                    <!-- begin::Login forgot password form -->
                    <div class="login-forgot">
                        <div class="mb-20">
                            <h3>{{__('Forgot Your Password?')}}</h3>
                            <div class="text-muted font-weight-bold">{{__('Enter your email to reset your password')}}</div>
                        </div>

                        <form method="POST" action="{{ route('password.email') }}" class="form" id="kt_login_forgot_form">
                            @csrf
                            <div class="form-group mb-10">
                                <input class="form-control form-control-solid h-auto py-4 px-8" type="text" placeholder="{{__('Email')}}" name="email" :value="old('email')" required autofocus autocomplete="off" />
                            </div>
                            <div class="form-group d-flex flex-wrap flex-center mt-10">
                                <button id="kt_login_forgot_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Request')}}</button>
                                <button id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">{{__('Cancel')}}</button>
                            </div>
                        </form>
                    </div>
                    <!-- end::Login forgot password form -->
                </div>
            </div>
        </div>
        <!-- end::Login -->
    </div>

    <script src="assets/js/pages/custom/login/login-general.js"></script>
</x-guest-layout>
