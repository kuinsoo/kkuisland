<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 15)->comment('이름');
            $table->string('identification', 30)->unique()->comment('ID');
            $table->string('phone', 15)->unique()->comment('연락처');
            $table->string('email', 50)->unique()->comment('이메일');
            $table->string('sido', 15)->nullable()->comment('시도');
            $table->string('sigungu', 50)->nullable()->comment('군구');
            $table->string('postcode', 15)->nullable()->comment('우편번호');
            $table->string('address1')->nullable()->comment('주소');
            $table->string('address2')->nullable()->comment('추가항목');
            $table->string('addressdetail')->nullable()->comment('상세주소');
            $table->string('agree')->comment('약관동의');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
