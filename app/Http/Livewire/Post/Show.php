<?php

namespace App\Http\Livewire\Post;

use App\Models\User;
use Livewire\Component;

class Show extends Component
{
    public $title;
    public $content;
    public $user;

    /**
     * *__construct()
     *
     * @param [type] $post
     * @return void
     */
    public function mount($post)
    {
        $this->title = $post->title;
        $this->content = $post->content;
    }

    public function like()
    {
        $this->user = User::where('id', 1)->first();
    }

    public function render()
    {
        return view('livewire.post.show');
    }
}
